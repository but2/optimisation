\documentclass{article}
\input{./forme_cours.tex}
 \def \equi#1{\mathrel{\mathop{\kern 0pt\sim}\limits_{#1}}}
%\usepackage[french]{babel}
\usepackage{polynom}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{tikz}

\title{Cours R4.04 Optimisation}
\author{FC}
\date{}

\begin{document}

\maketitle
\tableofcontents

\section{Préambule}

Le cours de théorie des graphes (R2.07) est un prérequis pour ce cours. Il est donc essentiel de revoir ce cours notamment pour les notions et le vocabulaire de la théorie des graphes (sommet, arc, chemin, connexité, degré...). Dans ce cours, pour un ensemble fini $E$, on notera $|E|=card(E)$, le nombre d'éléments de $E$.

\section{Introduction}

On veut faire transiter un maximum de marchandises du port $E$ au port $S$, sachant que les liaisons entre chaque port est limité par la quantité de marchandises pouvant transiter entre ces deux ports. On peut faire le même raisonnement pour faire voyager des personnes en train entre deux gares et les places disponibles dans chaque train. On modélise ce problème par un graphe orienté sans cycle, où chaque sommet est un port (ou une gare, ou autre chose...). On relie ces sommets par les liaisons possibles entre chaque port avec comme capacité sur les arcs la quantité de marchandise maximum pouvant transiter entre ces deux points. Voici un exemple de réseau :

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{$3$};
\draw[arc] (E)--(B) node[midway,below left]{$8$};
\draw[arc] (A)--(C) node[midway,above]{$4$};
\draw[arc] (B)--(D) node[midway,below]{$3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{$2$};
\draw[arc] (B)--(C) node[near end,below right]{$6$};
\draw[arc] (C)--(S) node[midway,above right]{$4$};
\draw[arc] (D)--(S) node[midway,below right]{$9$};
\end{tikzpicture}
\end{center}


Ce graphe a deux autres particularités :
\begin{itemize}
    \item Le sommet $e$ (source ou entrée) n'a que des arcs sortants (demi-degré intérieur égal à $0$).
    \item Le sommet $s$ (puit ou sortie) n'a que des arcs entrants (demi-degré extérieur égal à $0$).\\
\end{itemize}

Un réseau de transport doit également respecter les critères suivants :
\begin{itemize}
    \item La quantité transitée entre deux sommets doit être inférieure à la capacité de l'arc entre ces deux sommets.
    \item La quantité de marchandise arrivant à un sommet doit être égale à la quantité sortante (conservation).\\
\end{itemize}

Un exemple respectant ces critères est (en noir la capacité et en rouge la quantité de marchandise) :

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}3}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}4}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}3}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}3}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}0}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}1}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}4}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}3}/9$};
\end{tikzpicture}
\end{center}

Une question importante est de trouver la quantité maximale pouvant transiter entre $e$ et $s$.

\section{Réseaux de transports}

\subsection{Notion de flot dans un graphe}

L'idée du réseau de transport est la suivante : on dispose de deux ports que l'on nommera "source" et "puit" (on dit parfois "entrée" et "sortie"). On désire faire transiter entre ces deux ports une certaine quantité de marchandises. Ces marchandises vont transiter à travers d'autres ports pour arriver au port "sortie" (ou "puits"). On représente ce réseau par un graphe orienté sans cycle. Les ports sont donc reliés par des arcs valués par une certaine capacité (la quantité de marchandise maximale pouvant transiter entre ces deux ports). Le problème est de trouver la valeur maximale de marchandise que l'on peut faire passer du port "source" vers le port "puit".

\subsection{Notations}

\begin{Définition}[Réseau de transport]
Un réseau de transport est un graphe orienté sans cycle $G=(S,A,c)$ admettant les propriétés suivantes :
\begin{itemize}
    \item $G$ est valué par une capacité notée $c$, application de l'ensemble $A$ de ses arcs dans $\R_+^\star$. $c(a)$ désigne la capacité de l'arc $a$.
    \item Il existe dans S un sommet {\bf unique} noté $e$ comme "entrée" (ou "source") tel que $d^-(e)=0$.
    \item Il existe dans S un sommet {\bf unique} noté $s$ comme "sortie" (ou "puit") tel que $d^+(s)=0$.
\end{itemize}
\end{Définition}

\begin{Définition}[flot]
Un flot dans un réseau de transport $G$ est une valuation des arcs vérifiant les propriétés suivantes :
\begin{itemize}
    \item $\forall (i,j)\in A$, $f(i,j)\leq c(i,j)$
    \item $\forall x\in S\setminus\{e,s\}$, 
    \begin{equation}
         \sum_{a\in\Omega^+(x)}f(a)=\displaystyle\sum_{a\in\Omega^-(x)}f(a).
         \label{LoiCon}
     \end{equation}
\end{itemize}
\end{Définition}

Dans la définition précédente, $\Omega^+(x)=\{a\in A/I(a)=x\}$ et $\Omega^-(x)=\{a\in A/T(a)=x\}$. La propriété de la définition exprime une loi de conservation en chaque sommet du graphe, à savoir que tout ce qui entre en un sommet en ressort forcément !!
\begin{Définition}
Lorsque f est un flot, f(a) est le flux de l'arc a. La valeur du flot est 
$$
v(f) = \sum_{(e,i)\in A} f(e,i) = \sum_{(i,s)\in A}f(i,s).
$$
C'est cette dernière quantité que l'on cherche à maximiser.
\end{Définition}
\begin{rem}
La loi de Kirchhoff portant sur les intensités dans un réseau électrique considéré comme un graphe est un cas particulier de la loi de conservation.\\
\end{rem}


\begin{Définition}
Soit $f$ un flot dans un réseau de transport $G=(S,A,c)$.
\begin{itemize}
    \item Un arc $(i,j)$ est dit saturé si $f(i,j)=c(i,j)$
    \item Un flot est dit complet si tous les chemins de $e$ à $s$ ont un arc saturé.
    \item Un flot est dit maximal si sa valeur est maximale.
\end{itemize}
\end{Définition}

En fait, nous ne travaillerons pas avec un tel graphe $G=(S,A,c)$ mais avec le graphe suivant : on considère le graphe $G'=(S,A'=A\cup\{(s,e)\},c')$, où
\begin{itemize}
    \item Le graphe $G^\prime$ est le graphe $G$ complété par l'arc $(s,e)$.
    \item la capacité $c'$ est une application de $A$ dans $\bar{\N}^+$ telle que :
\begin{enumerate}
    \item $\forall a\in A,\, c'(a)=c(a)$.
    \item $c'(s,e)=+\infty$.
\end{enumerate}    
\end{itemize}

On remarque notamment que la loi de consevation peut s'écrire pour tout sommet de $G'$ et que, plus particulièrement,
$$v(f) = f(s,e)=\displaystyle\sum_{x\in\Omega^+(e)}f(e,x)=\displaystyle\sum_{y\in\Omega^-(s)}f(y,s),$$
soit tout ce qui part de "e" arrive en "s" (c'est la valeur du flot que l'on cherche à maximiser). \\

Si nous reprenons le réseau de l'introduction, cela donne

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}3}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}4}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}3}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}3}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}0}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}1}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}4}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}3}/9$};
\draw[arc] (S) to[bend left=90] node[pos=0.5,below]{${\color{red}7}/\infty$} (E);
\end{tikzpicture}
\end{center}

Dans cet exemple, le flot a pour valeur $7$. Est-ce le maximum ?

\section{Problème du flot maximum dans un réseau de transport}
Etant donné un réseau de transport $G=(S,A,c)$, il s'agit de trouver un flot $f^{\ast}$ du réseau $G'$ de valeur $v^{\ast}$ maximale (maximale par rapport à la valeur $v$ de tout autre flot du réseau). Ce problème est appelé problème du flot maximum et il revient à trouver un flot de valeur maximale.
\subsection{Algorithme de Ford et Fulkerson (1956)}
On conserve les notations précédentes. Lorsqu'un voyageur parcourt une chaîne {\bf élémentaire} d'extrémités {\bf e et s} en se déplaçant du sommet $e$ vers le sommet $s$, il rencontre deux sortes d'arcs : ceux qui sont orientés dans le sens de parcours du voyageur et qui vont constituer un ensemble $U$ et ceux qui sont orientés dans l'autre sens et qui forment un ensemble $V$.
\begin{Définition}
Une chaîne {\bf élémentaire} d'extrémités $e$ et $s$ est {\bf admissible} lorsque pour tout arc $a$ de la chaîne :\begin{enumerate}
    \item $a\in U\Longrightarrow f(a)<c(a)$.
    \item $a\in V\Longrightarrow f(a)>0$.
\end{enumerate}
\end{Définition}
Voici l'algorithme qui va nous permettre de réaliser un flot maximal :\\

\begin{Algorithme}[Ford Fulkerson]
\begin{enumerate}
    \item Soit $f$ un flot initial, par exemple $v(f)=0$.
    \item Tant qu'il existe une chaîne admissible élémentaire d'extrémités $e$ et $s$ faire :
    \begin{enumerate}
        \item $m :=Min(\displaystyle\min_{a\in U}(c(a)-f(a)), \,\min_{a\in V}(f(a))).$
        \item Pour chaque arc $a$ de la chaîne faire \\
\hspace*{2cm} $f(a)=\left\{\begin{array}{lll}
f(a)+m&{\rm si}&a\in U\\
f(a)-m&{\rm si}&a\in V\end{array}\right.$
    \end{enumerate}
\end{enumerate}
Lorsqu'il n'y a plus de chaîne admissible élémentaire, alors le flot est maximal.
\end{Algorithme}


\begin{Définition}[Flot complet]
Un flot est dit complet si tous les chemins de $e$ à $s$ ont un arc saturé.
\end{Définition}

\begin{Définition}[Flot maximal]
Un flot est maximal si sa valeur est maximale.
\end{Définition}

Pour bien comprendre cet algorithme, voici un exemple. Sur chacun des arcs est mentionnée la capacité (en noir). On initialise alors le flot à $0$ (en rouge sur les arcs).

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}0}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}0}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}0}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}0}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}0}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}0}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}0}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}0}/9$};
\draw[arc] (S) to[bend left=90] node[pos=0.5,below]{${\color{red}0}/\infty$} (E);
\end{tikzpicture}
\end{center}

Une première chaîne élémentaire admissible est : $e,B,C,s$. On calcule $m=\min(8,6,4)=4$. Voici le graphe à la fin de la première étape :

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}0}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}4}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}0}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}0}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}0}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}4}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}4}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}0}/9$};
\draw[arc] (S) to[bend left=90] node[pos=0.5,below]{${\color{red}4}/\infty$} (E);
\end{tikzpicture}
\end{center}

Une autre chaîne élémentaire admissible est : $e,B,D,s$. On calcule $m=\min(4,3,9)=3$ :

\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}0}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}7}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}0}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}3}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}0}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}4}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}4}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}3}/9$};
\draw[arc] (S) to[bend left=90] node[pos=0.5,below]{${\color{red}7}/\infty$} (E);
\end{tikzpicture}
\end{center}

Une autre chaîne élémentaire admissible est : $e,A,D,s$. On calcule $m=\min(3,2,6)=2$ :


\begin{center}
\begin{tikzpicture}
\tikzstyle{som}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[som] (E) at (-4,0) {e};
\node[som] (A) at (-1.5,1.5) {A};
\node[som] (B) at (-1.5,-1.5) {B};
\node[som] (C) at (1.5,1.5) {C};
\node[som] (D) at (1.5,-1.5) {D};
\node[som] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{${\color{red}2}/3$};
\draw[arc] (E)--(B) node[midway,below left]{${\color{red}7}/8$};
\draw[arc] (A)--(C) node[midway,above]{${\color{red}0}/4$};
\draw[arc] (B)--(D) node[midway,below]{${\color{red}3}/3$};
\draw[arc] (A)--(D) node[pos=0.25,below left]{${\color{red}2}/2$};
\draw[arc] (B)--(C) node[near end,below right]{${\color{red}4}/6$};
\draw[arc] (C)--(S) node[midway,above right]{${\color{red}4}/4$};
\draw[arc] (D)--(S) node[midway,below right]{${\color{red}5}/9$};
\draw[arc] (S) to[bend left=90] node[pos=0.5,below]{${\color{red}9}/\infty$} (E);
\end{tikzpicture}
\end{center}

L'algorithme se termine car il n'y a plus de chaîne admissible. Sur chacun des arcs du graphe, il est mentionné (en rouge) la valeur du flot. La valeur du flot maximal est donc $9$. 

\section{Théorème de la coupe minimale / Problème dual}
Voici tout d'abord quelques notations : $X$ et $Y$ désignent deux parties quelconques de l'ensemble $S$.\\
\indent - $(X,Y)$ désigne l'ensemble d'arcs $a$ tels que $I(a)\in X$ et $T(a)\in Y$.\\

\indent - $f(X,Y)=\displaystyle\sum_{a\in (X,Y)}f(a)$.\\

\indent - $c(X,Y)=\displaystyle\sum_{a\in (X,Y)}c(a)$.\\

\indent - Lorsque $X=\{x\}$, on posera $(X,Y)=(x,Y)$.  Lorsque $Y=\{y\}$, on posera $(X,Y)=(X,y)$. Ainsi, la valeur du flot s'écrit $v=f(S,s)=f(e,S)$.


\begin{Définition}
Soit $Y$ une partie de $S$ telle que $e\in Y$ et $s\not\in Y$. On pose $\bar{Y}=S-Y$ et donc $y \in \bar{Y}$. L'ensemble d'arcs $(Y,\bar{Y})$ est une {\bf coupe} du graphe $G$.
\end{Définition}

On a le théorème suivant.
\begin{Théorème}
Pour tout flot $f$ de valeur $v$ et toute coupe $(Y,\bar{Y})$ , $v=f(Y,\bar{Y})-f(\bar{Y},Y)$.
\end{Théorème}

\begin{Preuve}
Soit $(Y,\bar{Y})$ une coupe quelconque et $f$ un flot réalisable. La valeur du flot $v$ de $f$ s'écrit :
$$v=f(e,S)=f(e,S)+f(Y-\{e\},S)-f(Y-\{e\},S)=f(Y,S)-f(Y-\{e\},S).$$
D'après (\ref{LoiCon}) , $f(Y-\{e\},S)=f(S,Y-\{e\})$ puisque $e$ et $s$ n'appartiennent pas à $Y-\{e\}$. De plus, $d^-(e)$ est nul, ce qui implique $$f(Y-\{e\},S)=f(S,Y-\{e\})=f(S,Y).$$
On en déduit que $v=f(e,S)=f(Y,S)-f(S,Y)$. D'autre part, $S=Y\cup\bar{Y}$ et $Y\cap \bar Y=\emptyset$ soit alors
$$v=f(e,S)=f(Y,Y)+f(Y,\bar{Y})-f(Y,Y)-f(\bar{Y},Y)=f(Y,\bar{Y})-f(\bar{Y},Y).$$
\end{Preuve}

On peut en déduire le théorème suivant.

\begin{Théorème}\label{pro1}
Pour tout flot $f$ de valeur $v$ et toute coupe $(Y,\bar{Y})$, $v$ est toujours inférieure ou égale à la capacité de la coupe $c(Y,\bar{Y})$.
\end{Théorème}

\begin{Preuve}
Puisque $f$ est à valeurs positives, $f(\bar{Y},Y)$ est positif. De plus, $f$ étant réalisable, $f(Y,\bar{Y})$ est inférieur ou égal à $c(Y,\bar{Y})$, ce qui prouve le théorème.
\end{Preuve}


\begin{Théorème} 
La valeur $v^{\ast}$ d'un flot $f^{\ast}$ de valeur maximum est égale à la capacité d'une coupe de capacité minimale.
\end{Théorème}

\begin{Preuve}
Il reste à prouver que ce maximum est bien atteint et ce en une coupe $c(Y,\bar{Y})$.\\
Supposons avoir appliqué l'algorithme de Ford et Fulkerson jusqu'à son terme. Il n'existe donc plus de chaîne admissible d'extrémités $e$ et $s$. Soit $f^{\ast}$ le flot de valeur $v^{\ast}$ obtenu.\\

Soit $Z$ l'ensemble des sommets $x$ accessibles par une chaîne admissible dont l'autre extrémité est le sommet $e$. L'ensemble $(Z,\bar{Z})$ est une coupe (en effet, $e$ est dans $Z$ et $s$ n'est pas dans $Z$ car l'algorithme de Ford-Fulkerson est terminé).\\

S'il existait maintenant un arc $(t,u)$ appartenant à l'ensemble $(\bar{Z},Z)$ avec $f^{\ast}(t,u)>0$, il existerait une chaîne admissible d'extrémités $e$ et $t$, or $t$ est un élément de $\bar{Z}$, d'où la contradiction. On en déduit que :
$$\forall (t,u)\in  (\bar{Z},Z),\,f^{\ast}(\bar{Z},Z)=0.$$
D'après les deux propositions précédentes, $v^{\ast}=f^{\ast}(Z,\bar{Z})$ est toujours inférieure ou égale à $c(Z,\bar{Z})$. Le théorème sera démontré si on peut prouver que cette dernière inégalité est une égalité.
S'il existait maintenant un arc $(t,u)$ appartenant à l'ensemble $(Z,\bar{Z})$ avec $f^{\ast}(t,u)<c(t,u)$, il existerait une chaîne admissible d'extrémités $e$ et $u$, or $u$ est un élément de $\bar{Z}$, d'où la contradiction. On en déduit que :
$$\forall (t,u)\in  (Z,\bar{Z}),\,f^{\ast}(Z,\bar{Z})=c(Z,\bar{Z}).$$

Le théorème de la coupe minimale est démontré.
\end{Preuve}

En reprenant l'exemple introductif, on obtient après avoir effectué l'algorithme de Ford Fulkerson : $Z = \{e,A,B,C\}$ et $\bar Z = \{ D,s\}$ :

\begin{center}
\begin{tikzpicture}
\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZbar}=[circle,draw,fill=cyan!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZ] (E) at (-4,0) {e};
\node[somZ] (A) at (-1.5,1.5) {A};
\node[somZ] (B) at (-1.5,-1.5) {B};
\node[somZ] (C) at (1.5,1.5) {C};
\node[somZbar] (D) at (1.5,-1.5) {D};
\node[somZbar] (S) at (4,0) {s};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[arc] (E)--(A) node[midway,above left]{$3$};
\draw[arc] (E)--(B) node[midway,below left]{$8$};
\draw[arc] (A)--(C) node[midway,above]{$4$};
\draw[arc,color=blue] (B)--(D) node[midway,below]{$3$};
\draw[arc,color=blue] (A)--(D) node[pos=0.25,below left]{$2$};
\draw[arc] (B)--(C) node[near end,below right]{$6$};
\draw[arc,color=blue] (C)--(S) node[midway,above right]{$4$};
\draw[arc] (D)--(S) node[midway,below right]{$9$};
\end{tikzpicture}
\end{center}

La capacité de cette coupe $(Z,\bar Z)$ est 
$$
c(Z,\bar Z) = c(A,D)+c(B,D)+c(C,s)=2+3+4 = 9
$$
On retrouve la valeur du flot maximale.


\section{Couplage maximal dans un graphe biparti} % (fold)
\label{sec:couplage_maximal}


\begin{Définition}[Graphe biparti]
Un graphe $G=(S,A)$ est un graphe biparti si l'on peut séparer l'ensemble des sommets en deux parties disjointes, notées $U$ et $V$ (qui forment une partition de $S$), tels que chaque arête ait une extrémité dans $U$ et l'autre dans $V$.
\end{Définition}


\begin{Définition}[Couplage]
Un couplage d'un graphe est un ensemble d'arêtes deux à deux non adjacentes (pas de sommets en commun).
\end{Définition}

% Donner un exemple

\begin{Définition}[Couplage maximal]
Un couplage, $C$, est dit maximal (au sens de l'inclusion) si c'est un couplage tel que si on l'ajoute une arête $a$ à $C$, alors $C\cup\{a\}$ n'est plus un couplage.
\end{Définition}


\begin{ex}
Soit $G=(S,A)$ le graphe suivant :
% Donner un exemple de couplage maximal
\begin{center}
\begin{tikzpicture}
\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZbar}=[circle,draw,fill=cyan!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZ] (A) at (-4,0) {A};
\node[somZ] (B) at (-2,0) {B};
\node[somZ] (C) at (0,0) {C};
\node[somZ] (D) at (2,0) {D};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw (A) edge (B);
\draw[color=red] (B) edge (C);
\draw (C) edge (D);
\end{tikzpicture}
\end{center}

Le couplage (en rouge) est maximal mais non maximum (cf. figure suivante).
\end{ex}

\begin{Définition}[Couplage maximum]
Un couplage, $C$, est dit maximum s'il contient le plus grand nombre d'arêtes possibles, i.e. pour tout couplage $C^\prime$, on a $|C^\prime|\leq |C|$
\end{Définition}


\begin{ex}
Soit $G=(S,A)$ le graphe suivant :

\begin{center}
\begin{tikzpicture}
\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZbar}=[circle,draw,fill=cyan!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZ] (A) at (-4,0) {A};
\node[somZ] (B) at (-2,0) {B};
\node[somZ] (C) at (0,0) {C};
\node[somZ] (D) at (2,0) {D};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[color=red] (A) edge (B);
\draw (B) edge (C);
\draw[color=red] (C) edge (D);
\end{tikzpicture}
\end{center}
Ce couplage (en rouge) est maximum.
\end{ex}


% Donner un exemple de couplage maximum

\begin{Définition}[Couplage parfait]
Un couplage, $C$, est dit parfait si tous les sommets du graphe sont incidents à une arête de $C$.
\end{Définition}

\begin{ex}
Soit $G=(S,A)$ le graphe suivant :

\begin{center}
\begin{tikzpicture}
\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZbar}=[circle,draw,fill=cyan!50,text=blue]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZ] (A) at (-2,2) {A};
\node[somZ] (B) at (2,2) {B};
\node[somZ] (C) at (-2,-2) {C};
\node[somZ] (D) at (2,-2) {D};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw[color=red] (A) edge (B);
\draw (A) edge (C);
\draw (B) edge (D);
\draw[color=red] (C) edge (D);
\end{tikzpicture}
\end{center}
Le couplage (arêtes en rouge) est parfait.
\end{ex}

\begin{Définition}[Sommet saturé]
Un sommet $u$ est \em{saturé} par un couplage $C$ si $u$ est l'extrémité d'une arête de $C$.
\end{Définition}

\begin{Définition}[Chaîne alternée]
Si $C$ est un couplage d'un graphe $G$, on appelle \em{chaîne alternée} relativement à $C$ une chaîne élémentaire de $G$ dont les arêtes sont alternativement dans $C$ et hors de $C$.
\end{Définition}

% Donner un exemple !

\begin{Définition}[Chaîne augmentante]
Une chaîne alternée est dite \em{augmentante} si elle relie deux sommets non saturés.
\end{Définition}

% Donner un exemple !
\begin{re}
Si on a une chaîne M-alternée augmentante, alors, on peut trouver un couplage de cardinalité plus grande : on  échange dans la chaîne les arêtes du couplage et celles qui n'y sont pas.
\end{re}

\begin{Théorème}[Théorème de Hall ou lemme des mariages]
Un graphe biparti $G = (S,A)$ avec $S=U\cup V$ et $U$ et $V$ formant une partition de $S$ et $|U|=|V|$,  admet un couplage parfait si et seulement si pour tout sous-ensemble X de U (de V, respectivement), le nombre de sommets de V (de U, respectivement) adjacents à X est supérieur ou égal à la cardinalité de X.
\end{Théorème}

% Donner un exemple

Pour trouver un couplage maximum dans un graphe biparti, on ajoute deux sommets, $e$ et $s$, et on se ramène à un problème de flot sur un réseau, en mettant une capacité de $1$ sur chaque arc. Le couplage maximum correspond à l'ensemble des arêtes saturées après avoir effectué l'algorithme de Ford-Fulkerson.

% Donner un exemple


\subsection{Transversal}

\begin{Définition}[Transversal]
Soit $G=(S,A)$ un graphe. $T\subset S$ est un transversal de $G$ si toute arête de $A$ possède au moins une extrémité dans $T$. 
\end{Définition}

\begin{Définition}[Transversal minimal]
Soit $G=(S,A)$ un graphe. $T$ est un transversal minimal de $G$ si quand l'on retire n'importe quel sommet de $T$ alors le nouvel ensemble n'est plus un transversal de $G$, i.e. $\forall x\in T$, $T\setminus\{x\}$ n'est pas un transversal de $G$.
\end{Définition}


\begin{ex}
Soit $G=(S,A)$ le graphe suivant :

\begin{center}
\begin{tikzpicture}
%\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZ}=[circle,draw]
\tikzstyle{somZcol}=[circle,draw,fill=cyan!50]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZcol] (A) at (-3,0) {A};
\node[somZ] (B) at (0,3) {B};
\node[somZcol] (C) at (3,0) {C};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw (A) edge (B);
\draw (B) edge (C);
\end{tikzpicture}
\end{center}

$T=\{A,C\}$ est un transversal minimal (sommet en bleu) mais non minimum (cf. figure suivante).

\end{ex}


\begin{Définition}[Transversal minimum]
Soit $G=(S,A)$ un graphe. $T$ est un transversal minimum de $G$ si pour tout transversal $T^\prime$ alors $|T|\leq |T^\prime|$.
\end{Définition}


\begin{ex}
Soit $G=(S,A)$ le graphe suivant :
\begin{center}
\begin{tikzpicture}
%\tikzstyle{somZ}=[circle,draw,fill=yellow!50,text=blue]
\tikzstyle{somZ}=[circle,draw]
\tikzstyle{somZcol}=[circle,draw,fill=cyan!50]
\tikzstyle{arc}=[->,very thick,>=latex]
\node[somZ] (A) at (-3,0) {A};
\node[somZcol] (B) at (0,3) {B};
\node[somZ] (C) at (3,0) {C};
%\draw[arc] (S)--(A) node[midway,right]{$1$} node[midway,left]{$\color{red} 2$};
\draw (A) edge (B);
\draw (B) edge (C);
\end{tikzpicture}
\end{center}

$T=\{B\}$ est un transversal minimum.
\end{ex}

\subsection{Lien Transversal et couplage}

\begin{Théorème}
Soit $G=(S,A)$. Pour tout transversal $T$ de $G$ et pour tout couplage $C$ de $G$, on a $|C|\leq |T|$.
\end{Théorème}

\begin{Théorème}[König]
S'il existe un couplage $C$ et un transversal $T$ d'un graphe $G$, tels que $|C|=|T|$ alors $C$ est un couplage maximum et $T$ est un transversal minimum de $G$.
\end{Théorème}

\subsection{Problème d'affectation}

On suppose que l'on a un graphe biparti avec $2n$ sommets. On souhaite affecter $n$ tâches à $n$ personnes (1 tâche par personne). On représente les coûts par une matrice $n\times n$. On souhaite affecter ces tâches en minimisant le coût total.

On peut appliquer l'algorithme de Kuhn (ou méthode hongroise) : \\

\begin{itemize}
    \item {\bf Etape 0} : On retire à chaque ligne le minimum de chaque ligne et à chaque colonne son minimum.\\

    \item {\bf Etape 1 : on encadre et barre des zéros}. \\
    \begin{enumerate}
        \item[a)] On cherche la ligne contenant le moins de zéros non barrés (prendre la première en partant du haut si il y en a plusieurs).\\
        \item[b)] On encadre un zéro (le plus à gauche), puis on barre tous les zéros sur la ligne et la colonne du zéro encadré.\\
        \item[c)] On recommence l'opération jusqu'à ce qu'on ne puisse plus encadrer, ni barrer de zéros.\\
    \end{enumerate} 
    Si l'on a encadré un zéro par ligne et par colonne, c'est terminé, on a la solution optimale, sinon on passe à l'étape 2.\\
    \item {\bf Etape 2 : On marque des lignes et des colonnes}\\
    \begin{enumerate}
        \item[a)] On marque toute ligne sans zéro encadré.\\
        \item[b)] On marque toute colonne ayant un zéro barré sur une ligne marquée.\\
        \item[c)] On marque tous les lignes avec un zéro encadré dans une colonne marquée et ainsi de suite.\\
    \end{enumerate}
        On répète b) et c) jusqu'à ne plus pouvoir marquer de rangée. A la fin, {\bf on barre les lignes non marquées et les colonnes marquées}.\\
    \item {\bf Etape 3 : } on prend le minimum de toutes les cases non barrées, et on retranche à ces cases le minimum de celles-ci et on ajoute ce même minimum à toutes les cases barrées deux fois (en ligne et en colonne).\\
    On obtient alors un nouveau tableau sur lequel on pourra répéter la succession des étapes $1$ à $3$.\\
\end{itemize}

\begin{ex}
Nous disposons de 5 machines pour effectuer 5 tâches, où les coûts sont donnés dans le tableau suivant :

\begin{table}[hb!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& $T_1$ &$T_2$ &$T_3$ &$T_4$ &$T_5$ \\ 
\hline
$M_1$ &17 & 15 & 9 & 5 & 12\\
\hline
$M_2$ &16 & 16 & 10 & 5 & 10\\
\hline
$M_3$ &12 & 15 & 14 & 11 & 5\\
\hline
$M_4$ &4 & 8 & 14 & 17 & 13\\
\hline
$M_5$& 13 & 9 & 8 & 12 & 17\\
\hline
\end{tabular}
\end{table}

Ainsi, le temps d'exécution de la machine $M_3$ pour réaliser la tâche $T_4$ est de $11$. On souhaite affecter chaque tâche à chaque machine en minimisant le coût, i.e. le temps d'execution. On va donc appliquer la méthode hongroise sur cette matrice.


{\bf Etape 0 : On enlève à chaque ligne son minimum puis à chaque colonne son minimum}

\begin{table}[htb]
    \centering
    \hfill
    \begin{tabular}{|c|c|c|c|c|}
\hline
12 & 10 & 4 & 0 & 7\\
\hline
11 & 11 & 5 & 0 & 5\\
\hline
7 & 10 & 9 & 6 & 0\\
\hline
0 & 4 & 10 & 13 & 9\\
\hline
5 & 1 & 0 & 4 & 9\\
\hline
\end{tabular}
    \hfill
    \begin{tabular}{|c|c|c|c|c|}
\hline
12 & 9 & 4 & $0$ & 7\\
\hline
11 & 10 & 5 & 0 & 5\\
\hline
7 & 9 & 9 & 6 & 0\\
\hline
0 & 3 & 10 & 13 & 9\\
\hline
5 & 0 & 0 & 4 & 9\\
\hline
\end{tabular}
    \hfill\null
    \caption{A gauche, on a retranché le minimum sur chaque ligne puis à droite on a fait de même sur les colonnes}
    \label{uca}
\end{table}

{\bf Etape 1 : on encadre et barre des zéros}. On cherche la ligne contenant le moins de zéros non barrés. Ici c'est la première. On encadre le zéro puis on barre les zéros sur la 1ère ligne et 4ème colonne (tableau de gauche) puis on prend ensuite la 3ème ligne (rien à barrer), puis la 4ème et enfin la 5ème. Sur cette dernière, nous devons barrer le zéro en 3ème colonne (tableau de droite) :

\begin{table}[htb!]
\centering
\hfill
\begin{tabular}{|c|c|c|c|c|}
\hline
12 & 9 & 4 & $\boxed{0}$ & 7\\
\hline
11 & 10 & 5 & $\not 0$ & 5\\
\hline
7 & 9 & 9 & 6 & 0\\
\hline
0 & 3 & 10 & 13 & 9\\
\hline
5 & 0 & 0 & 4 & 9\\
\hline
\end{tabular}
\hfill
\begin{tabular}{|c|c|c|c|c|}
\hline
12 & 9 & 4 & $\boxed{0}$ & 7\\
\hline
11 & 10 & 5 & $\not 0$ & 5\\
\hline
7 & 9 & 9 & 6 & $\boxed{0}$\\
\hline
$\boxed{0}$ & 3 & 10 & 13 & 9\\
\hline
5 & $\boxed{0}$ & $\not 0$ & 4 & 9\\
\hline
\end{tabular}
    \hfill\null
    %\caption{A gauche, on a retranché le minimum sur chaque ligne puis à droite on a fait de même sur les colonnes}
    \label{ucb}
\end{table}


\newpage

{\bf Etape 2 : on marque des lignes et des colonnes}. On marque toute ligne sans zéro encadré, ici la deuxième ligne (croix en marge). On marque toute colonne ayant un zéro barré sur une ligne marquée (soit la 4ème colonne en rouge). Puis, on marque tous les lignes avec un zéro encadré dans une colonne marquée (soit la première ligne).


\begin{table}[hb!]
\centering
\hfill
\begin{tabular}{|c|c|c|c|c|c}
\hline
12 & 9 & 4 & $\boxed{0}$ & 7&x\\
\hline
11 & 10 & 5 & $\not 0$ & 5& x\\
\hline
7 & 9 & 9 & 6 & $\boxed{0}$\\
\hline
$\boxed{0}$ & 3 & 10 & 13 & 9\\
\hline
5 & $\boxed{0}$ & $\not 0$ & 4 & 9\\
\hline
& & & x &
\end{tabular}
\hfill
\begin{tabular}{|c|c|c|>{\columncolor{gray}}c|c|}
\hline
12 & 9 & 4 & $\boxed{0}$ & 7\\
\hline
 11 & 10 & 5 & $\not 0$ & 5\\
\hline
\rowcolor{gray}7 & 9 & 9 & \cellcolor{darkgray}6 & $\boxed{0}$\\
\hline
\rowcolor{gray}$\boxed{0}$ & 3 & 10 & \cellcolor{darkgray}13 & 9\\
\hline
\rowcolor{gray}5 & $\boxed{0}$ & $\not 0$ & \cellcolor{darkgray}4 & 9\\
\hline
\end{tabular}
  \hfill\null
    \caption{En gris foncé sont les cases barrés deux fois (ligne et colonne)}
    \label{ucc}
\end{table}


{\bf Etape 3 : on prend le minimum de toutes les cases non barrées, et on retranche à ces cases le minimum de celles-ci (ici 4) et on ajoute ce même minimum à toutes les cases barrées deux fois (en ligne et en colonne)} (tableau de gauche), puis on réitère l'étape $1$ sur ce nouveau tableau (tableau de droite) : 

\begin{table}[hb!]
\centering
\hfill
\begin{tabular}{|c|c|c|c|c|}
\hline
8 & 5 & 0 & $0$ & 3\\
\hline
7 & 6 & 1 & $0$ & 1\\
\hline
7 & 9 & 9 & 10 & $0$\\
\hline
$0$ & 3 & 10 & 17 & 9\\
\hline
5 & $0$ & $0$ & 8 & 9\\
\hline
\end{tabular}
\hfill
\begin{tabular}{|c|c|c|c|c|}
\hline
8 & 5 & $\boxed{0}$ & $\not 0$ & 3\\
\hline
7 & 6 & 1 & $\boxed{0}$ & 1\\
\hline
7 & 9 & 9 & 10 & $\boxed{0}$\\
\hline
$\boxed{0}$ & 3 & 10 & 17 & 9\\
\hline
5 & $\boxed{0}$ & $\not 0$ & 8 & 9\\
\hline
\end{tabular}
  \hfill\null
   % \caption{En gris foncé sont les cases barrés deux fois (ligne et colonne)}
    \label{ucd}
\end{table}

On remarque que le tableau possède exactement 1 zéro encadré dans chaque ligne et chaque colonne et donc l'algorithme est terminé. En se ramenant au tableau initial, on a 


\begin{table}[htb]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& $T_1$ &$T_2$ &$T_3$ &$T_4$ &$T_5$ \\ 
\hline
$M_1$ &17 & 15 & $\boxed{9}$ & 5 & 12\\
\hline
$M_2$ &16 & 16 & 10 & $\boxed{5}$ & 10\\
\hline
$M_3$ &12 & 15 & 14 & 11 & $\boxed{5}$\\
\hline
$M_4$ &$\boxed{4}$ & 8 & 14 & 17 & 13\\
\hline
$M_5$& 13 & $\boxed{9}$ & 8 & 12 & 17\\
\hline
\end{tabular}
\end{table}

\newpage
Ainsi, 
\begin{itemize}
    \item la machine 1 effectuera la tâche 3 pour un coût de 9
    \item la machine 2 effectuera la tâche 4 pour un coût de 5
    \item la machine 3 effectuera la tâche 5 pour un coût de 5
    \item la machine 4 effectuera la tâche 1 pour un coût de 4
    \item la machine 5 effectuera la tâche 2 pour un coût de 9
\end{itemize}
soit un coût total de 32.
\end{ex}

\begin{re}
Pour un même problème de maximisation, on pourra refaire la même méthode en changeant tous les nombres par le complémentaire au maximum de ces nombres. Ainsi, l'algorithme sera appliqué à ce nouveau tableau :

\begin{table}[hb!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& $T_1$ &$T_2$ &$T_3$ &$T_4$ &$T_5$ \\ 
\hline
$M_1$ &0 & 2 & 8 & 12 & 5\\
\hline
$M_2$ &1 & 1 & 7 & 12 & 7\\
\hline
$M_3$ &5 & 2 & 3 & 6 & 12\\
\hline
$M_4$ &13 & 9 & 3 & 0 & 4\\
\hline
$M_5$& 4 & 8 & 9 & 5 & 0\\
\hline
\end{tabular}
\end{table}

On retire le minimum de chaque ligne et chaque colonne :

\begin{table}[hb!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& $T_1$ &$T_2$ &$T_3$ &$T_4$ &$T_5$ \\ 
\hline
$M_1$ &$\boxed{0}$ & 2 & 7 & 12 & 5\\
\hline
$M_2$ &$\not 0$ & $\boxed{0}$ & 5 & 11 & 6\\
\hline
$M_3$ &3 & $\not 0$ & $\boxed{0}$ & 4 & 10\\
\hline
$M_4$ &13 & 9 & 2 & $\boxed{0}$ & 4\\
\hline
$M_5$& 4 & 8 & 8 & 5 & $\boxed{0}$\\
\hline
\end{tabular}
\end{table}

L'algorithme est fini car nous avons encadré exactement un zéro par ligne et par colonne. En revenant au tableau initial, on a


\begin{table}[hb!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& $T_1$ &$T_2$ &$T_3$ &$T_4$ &$T_5$ \\ 
\hline
$M_1$ &$\boxed{17}$ & 15 & 9 & 5 & 12\\
\hline
$M_2$ &16 & $\boxed{16}$ & 10 & 5 & 10\\
\hline
$M_3$ &12 & 15 & $\boxed{14}$ & 11 & 5\\
\hline
$M_4$ &4 & 8 & 14 & $\boxed{17}$ & 13\\
\hline
$M_5$& 13 & 9 & 8 & 12 & $\boxed{17}$\\
\hline
\end{tabular}
\end{table}


Ainsi, 
\begin{itemize}
    \item la machine 1 effectuera la tâche 1 pour un coût de 17
    \item la machine 2 effectuera la tâche 2 pour un coût de 16
    \item la machine 3 effectuera la tâche 3 pour un coût de 14
    \item la machine 4 effectuera la tâche 4 pour un coût de 17
    \item la machine 5 effectuera la tâche 5 pour un coût de 17
\end{itemize}
soit un coût total de 81.


\end{re}








% Regarder le problème d'affectation

% Méthode hongroise

% Algo d'Edmonds pour les couplages

% section couplage_maximal (end)











\end{document}
